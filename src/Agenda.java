import java.util.ArrayList;
import java.util.Scanner;

public class Agenda {
	
	ArrayList<Contacto>  contactos;

	public ArrayList<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(ArrayList<Contacto> contactos) {
		this.contactos = contactos;
	}

	public Agenda(ArrayList<Contacto> contactos) {
		super();
		this.contactos = contactos;
	}

	public Agenda() {
		super();

		contactos=new ArrayList<Contacto>();
	}

	@Override
	public String toString() {
		
		System.out.println("\n\n\t\t5-Listado de contactos");
		for (Contacto aux : contactos) {
			System.out.println(aux.toString());  
		}
		return null;
	}

	public void aniadirContacto(Contacto nuevo) {
		
		contactos.add(nuevo);
		
	}

	public void altaContacto() {
		Scanner sc=new Scanner(System.in);
		
		System.out.println("\n\n\t\t1-Nuevo contacto");
		System.out.println("Por favor introduzca el nombre:");
		String nombre = sc.nextLine();
		System.out.println("Por favor introduzca el apellido:");
		String apellido = sc.nextLine();
		System.out.println("Por favor introduzca el telefono:");
		String telefono = sc.nextLine();
		
		Contacto nuevo = new Contacto(nombre, apellido, telefono);
		
		contactos.add(nuevo);
		
	}

	public void buscarContacto() {
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("\n\n\t\t2-Buscar contacto");
		System.out.println("Nombre a buscar:");
		String nombre = sc.nextLine();
		int pos = -1;
		for (Contacto aux : contactos) {

			if (aux.getNombre().equalsIgnoreCase(nombre)) {
				System.out.println("CONTACTO ENCONTRADO ");

				pos = contactos.indexOf(aux);

				System.out.println("LOS DATOS DEL CONTACTO SON : " + aux.getNombre() + "\t" + aux.getApellido()
						+ "\t" + aux.getTelefono());
			}
		}
		if (pos == -1) {
			System.out.println("CONTACTO NO SE HA ENCONTRADO ");
		}

		
	}

	public void modificarContacto() {
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("\n\n\t\t2-Modificar contacto");
		System.out.println("Nombre a buscar:");
		String nombre = sc.nextLine();
		int pos = -1;
		for (Contacto aux : contactos) {

			if (aux.getNombre().equalsIgnoreCase(nombre)) {

				pos = contactos.indexOf(aux);

				System.out.println("ELIGE LO QUE QUIERES MODIFICAR : " + "\n\t\t\t1.-" + aux.getNombre()
						+ "\n\t\t\t2.-" + "\t" + aux.getApellido() + "\n\t\t\t3.-" + "\t" + aux.getTelefono());

				int campo = sc.nextInt();
				sc.nextLine();
				switch (campo) {
				case 1:
					System.out.println("NUEVO NOMBRE : ");
					aux.setNombre(sc.nextLine());
					break;
				case 2:
					System.out.println("NUEVO APELLIDO : ");
					aux.setApellido(sc.nextLine());
					break;
				case 3:
					System.out.println("NUEVO TELEFONO : ");
					aux.setTelefono(sc.nextLine());
					break;
				default:
				}
			}
		}
		if (pos == -1) {
			System.out.println("CONTACTO NO SE HA ENCONTRADO ");
		}
		
	}

	public void borrarContacto() {
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("\n\n\t\t4-Eliminar un contacto");
		System.out.println("Nombre a borrar:");
		String nombre = sc.nextLine();
		// pos=-1;
		// for (Contacto aux:agenda) {
		//
		// if (aux.getNombre().equalsIgnoreCase(nombre)) {
		// System.out.println("CONTACTO PARA BORRAR ENCONTRADO ");
		//
		// pos=agenda.indexOf(aux);
		// }
		// }
		// if (pos==-1) {
		// System.out.println("CONTACTO NO SE HA ENCONTRADO ");
		// } else {
		// agenda.remove(pos);
		// System.out.println("CONTACTO BORRADO ");
		// }
		// OTRA FORMA DE HACER LOMISMO
		boolean encontrado = false;

		for (int i = 0; (i < contactos.size() && encontrado == false); i++) {
			
			if (contactos.get(i).getNombre().equalsIgnoreCase(nombre)) {

				contactos.remove(i);
				encontrado = true;
			}
		}
		if (!encontrado) {
			System.out.println("CONTACTO BORRADO ");
		}
		
	}
	
	
	

}
